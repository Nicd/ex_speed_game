# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
import Config

button_pins = {24, 25, 5, 6}
buttons = 4
leds_per_button = 7
brightness = 45

config :ex_speed_game,
  target: Mix.target(),

  # Set to true to enable debug prints (via serial console) and false to disable.
  debug: true,

  # Amount of buttons in the game.
  buttons: buttons,

  # How many LEDs are in the NeoPixel chain inside each button
  leds_per_button: leds_per_button,

  # Colors of the LEDs keyed by index
  led_colors: %{
    0 => {31, 133, 222},
    1 => {65, 222, 31},
    2 => {241, 229, 0},
    3 => {250, 51, 51}
  },

  # Initial brightness of the LEDs
  led_brightness: brightness,

  # LED PWM channel
  led_channel: 0,

  # Pins of the buttons, in order from left to right in the case. Leftmost will be 0, and number
  # will increase to the right.
  button_pins: button_pins,

  # Debounce delay, i.e. how long a button must be high or low before it is accepted, to reduce
  # spurious inputs. In milliseconds.
  debounce_delay: 20,

  # Delay between repeated presses of the same button. Any repeated presses during this duration
  # after the initial press will be discarded, to prevent double hits from unreliable buttons.
  # In milliseconds.
  repeat_delay: 100,

  # Delay at start of game between ticks, in milliseconds.
  delay_start: 570,

  # Score at start of pro game
  score_pro: 100,

  # Maximum amount of ticks you can be "behind" before the game is stopped.
  max_queue: 20,

  # Time to show win LEDs for at minimum
  win_delay: 2_000,

  # Interface to show IP for when using ShowIP
  iface: "wlan0",

  # LED pattern to show when there is a crash
  crash_pattern: [false, true, true, false],

  # Time to show crash pattern for
  crash_pattern_delay: 3_000,

  # LED pattern to show when game ends
  fail_pattern: [true, true, true, true]

# Customize non-Elixir parts of the firmware. See
# https://hexdocs.pm/nerves/advanced-configuration.html for details.

config :nerves, :firmware, rootfs_overlay: "rootfs_overlay"

# Set the SOURCE_DATE_EPOCH date for reproducible builds.
# See https://reproducible-builds.org/docs/source-date-epoch/ for more information

config :nerves, source_date_epoch: "1583172807"

# Use Ringlogger as the logger backend and remove :console.
# See https://hexdocs.pm/ring_logger/readme.html for more information on
# configuring ring_logger.

config :logger, backends: [RingLogger]

config :ex_speed_game, ExSpeedGame.Web.Endpoint,
  url: [host: "localhost"],
  http: [port: 1337],
  debug_errors: true,
  check_origin: false,
  secret_key_base: "s65CvlHa8RM4jVIgqCAbpAwbuiUFSJr7FUy5mh7kB0UrnSpOEcAcDu1Eajgtm912",
  render_errors: [view: ExSpeedGame.Web.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: ExSpeedGame.Web.PubSub,
  live_view: [signing_salt: "58shuVUu"],
  server: true

config :phoenix, :json_library, Jason
config :phoenix, :stacktrace_depth, 20

gamma = [
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   1,   1,
  1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   2,   2,   2,   2,   2,   2,
  2,   3,   3,   3,   3,   3,   3,   3,   4,   4,   4,   4,   4,   5,   5,   5,
  5,   6,   6,   6,   6,   7,   7,   7,   7,   8,   8,   8,   9,   9,   9,  10,
 10,  10,  11,  11,  11,  12,  12,  13,  13,  13,  14,  14,  15,  15,  16,  16,
 17,  17,  18,  18,  19,  19,  20,  20,  21,  21,  22,  22,  23,  24,  24,  25,
 25,  26,  27,  27,  28,  29,  29,  30,  31,  32,  32,  33,  34,  35,  35,  36,
 37,  38,  39,  39,  40,  41,  42,  43,  44,  45,  46,  47,  48,  49,  50,  50,
 51,  52,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,  64,  66,  67,  68,
 69,  70,  72,  73,  74,  75,  77,  78,  79,  81,  82,  83,  85,  86,  87,  89,
 90,  92,  93,  95,  96,  98,  99, 101, 102, 104, 105, 107, 109, 110, 112, 114,
115, 117, 119, 120, 122, 124, 126, 127, 129, 131, 133, 135, 137, 138, 140, 142,
144, 146, 148, 150, 152, 154, 156, 158, 160, 162, 164, 167, 169, 171, 173, 175,
177, 180, 182, 184, 186, 189, 191, 193, 196, 198, 200, 203, 205, 208, 210, 213,
215, 218, 220, 223, 225, 228, 231, 233, 236, 239, 241, 244, 247, 249, 252, 255
]

config :blinkchain, canvas: {buttons * leds_per_button, 1}

config :blinkchain, dma_channel: 10

config :blinkchain, :channel0,
  pin: 18,
  type: :grb,
  brightness: brightness,
  gamma: gamma,
  arrangement: [
    %{
      type: :strip,
      origin: {0, 0},
      count: buttons * leds_per_button,
      direction: :right
    }
  ]

if Mix.target() != :host do
  import_config "target.exs"
end
