defmodule ExSpeedGame.Web.MainLive.Main do
  use ExSpeedGame.Web, :live_view
  require Logger

  alias ExSpeedGame.PubSub
  alias ExSpeedGame.Game.{Menu}
  alias ExSpeedGame.Game.Modes.{Speed}

  @impl true
  def mount(_params, _session, socket) do
    menu_state = PubSub.subscribe(ExSpeedGame.Game.Menu)

    socket = assign(socket, score: 0, queue_len: 0)

    {:ok, set_menu_assigns(socket, menu_state)}
  end

  @impl true
  def handle_info(msg, socket)

  def handle_info({:esg_pubsub, :pub, %Menu.State{} = menu_state}, socket) do
    socket =
      if socket.assigns.mode == :menu and menu_state.mode == :ingame do
        set_game_assigns(socket, PubSub.subscribe(menu_state.game))
      else
        socket
      end

    {:noreply, set_menu_assigns(socket, menu_state)}
  end

  def handle_info({:esg_pubsub, :pub, %Speed.State{} = game_state}, socket) do
    {:noreply, set_game_assigns(socket, game_state)}
  end

  def handle_info(msg, socket) do
    Logger.debug("Got unknown message into LV: #{inspect(msg)}")
    {:noreply, socket}
  end

  defp set_menu_assigns(socket, %Menu.State{mode: mode, index: index}) do
    assign(socket, mode: mode, title: Menu.index_name(index))
  end

  defp set_game_assigns(socket, %Speed.State{} = game_state) do
    assign(socket,
      mode: :ingame,
      score: game_state.score,
      queue_len: game_state.queue_len
    )
  end
end
