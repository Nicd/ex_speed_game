defmodule ExSpeedGame.Web.Router do
  use ExSpeedGame.Web, :router

  import Phoenix.LiveDashboard.Router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_live_flash)
    plug(:put_root_layout, {ExSpeedGame.Web.LayoutView, :root})
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", ExSpeedGame.Web do
    pipe_through(:browser)

    live("/", MainLive.Main, :main)
    live_dashboard("/dashboard", metrics: ExSpeedGame.Web.Telemetry)
  end

  # Other scopes may use custom stacks.
  # scope "/api", ExSpeedGame.Web do
  #   pipe_through :api
  # end
end
