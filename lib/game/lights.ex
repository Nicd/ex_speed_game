defmodule ExSpeedGame.Game.Lights do
  use GenServer
  use Bitwise, only_operators: true
  import ExSpeedGame.Utils.TypedStruct

  require Logger

  @type values :: [boolean()]
  @leds_per_button Application.compile_env!(:ex_speed_game, :leds_per_button)
  @led_colors Application.compile_env!(:ex_speed_game, :led_colors)
  @black {0, 0, 0}

  defmodule Options do
    deftypedstruct(%{
      name: GenServer.name(),
      channel: Blinkchain.channel_number(),
      brightness: 0..255
    })
  end

  defmodule State do
    deftypedstruct(%{
      channel: Blinkchain.channel_number(),
      brightness: 0..255
    })
  end

  ### SERVER INTERFACE

  @spec start_link(Options.t()) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(%Options{} = opts) do
    GenServer.start_link(
      __MODULE__,
      Map.from_struct(opts),
      name: opts.name
    )
  end

  @impl true
  @spec init(%{channel: Blinkchain.channel_number(), brightness: 0..255}) :: {:ok, State.t()}
  def init(%{channel: channel, brightness: brightness}) do
    Blinkchain.set_brightness(channel, brightness)

    {:ok, %State{channel: channel, brightness: brightness}}
  end

  @impl true
  def handle_call(msg, from, state)

  def handle_call({:set_lights, values}, _from, %State{} = state) do
    values
    |> Enum.with_index()
    |> Enum.each(fn
      {true, i} ->
        from = index2coordinates(i)
        Blinkchain.fill(from, @leds_per_button, 1, index2color(i))

      {false, i} ->
        from = index2coordinates(i)
        Blinkchain.fill(from, @leds_per_button, 1, @black)
    end)

    Logger.debug(fn -> "Setting lights #{inspect(values)}" end)

    Blinkchain.render()

    {:reply, :ok, state}
  end

  def handle_call({:set_brightness, brightness}, _from, %State{} = state) do
    Blinkchain.set_brightness(state.channel, brightness)
    Blinkchain.render()

    {:reply, :ok, %State{state | brightness: brightness}}
  end

  def handle_call(:get_brightness, _from, %State{} = state) do
    {:reply, state.brightness, state}
  end

  ### CLIENT INTERFACE

  @doc """
  Set the lights on/off according to the given value.
  """
  @spec set(GenServer.name(), values()) :: :ok
  def set(server, values) do
    :ok = GenServer.call(server, {:set_lights, values})
  end

  @doc """
  Set the brightness of the lights from 0 (off) to 255 (max).
  """
  @spec set_brightness(GenServer.name(), 0..255) :: :ok
  def set_brightness(server, brightness) do
    :ok = GenServer.call(server, {:set_brightness, brightness})
  end

  @doc """
  Get the brightness of the lights from 0 (off) to 255 (max).
  """
  @spec get_brightness(GenServer.name()) :: 0..255
  def get_brightness(server) do
    GenServer.call(server, :get_brightness)
  end

  @doc """
  Turn off all lights.
  """
  @spec clear(GenServer.name()) :: :ok
  def clear(server) do
    set(server, [false, false, false, false])
  end

  @doc """
  Show the given number on the lights as a binary pattern.
  """
  @spec show_binary(GenServer.name(), integer()) :: :ok
  def show_binary(server, number) do
    values = number2binary(number)
    set(server, values)
  end

  @doc """
  Set the given index on and the others off.
  """
  @spec set_index(GenServer.name(), integer()) :: :ok
  def set_index(server, index) do
    set(server, [index == 1, index == 2, index == 3, index == 4])
  end

  @spec number2binary(integer()) :: values()
  defp number2binary(number) do
    [
      (number &&& 0b1000) != 0,
      (number &&& 0b0100) != 0,
      (number &&& 0b0010) != 0,
      (number &&& 0b0001) != 0
    ]
  end

  @spec index2coordinates(0..3) :: Blinkchain.Point.t()
  defp index2coordinates(index) do
    %Blinkchain.Point{
      x: index * 7,
      y: 0
    }
  end

  @spec index2color(0..3) :: {pos_integer(), pos_integer(), pos_integer()}
  defp index2color(index) do
    Map.fetch!(@led_colors, index)
  end
end
