defmodule ExSpeedGame.Game.Types do
  @type pin :: Circuits.GPIO.pin_number()
  @type pins :: {pin(), pin(), pin(), pin()}
  @type choice :: 1..4
end
