defmodule ExSpeedGame.Game.ChangeBrightness do
  use GenServer, restart: :temporary
  import ExSpeedGame.Utils.TypedStruct
  require Logger

  alias ExSpeedGame.Game.Lights
  alias ExSpeedGame.Game.ButtonInput

  @brightness_step 10

  defmodule Options do
    deftypedstruct(%{
      input_server: GenServer.name(),
      lights_server: GenServer.name()
    })
  end

  defmodule State do
    deftypedstruct(%{
      input_server: GenServer.name(),
      lights_server: GenServer.name(),
      brightness: 0..255
    })
  end

  @spec start_link(Options.t()) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(%Options{} = opts) do
    GenServer.start_link(__MODULE__, Map.from_struct(opts))
  end

  @impl true
  @spec init(%{input_server: GenServer.name(), lights_server: GenServer.name()}) ::
          {:ok, State.t()}
  def init(%{input_server: input_server, lights_server: lights_server}) do
    ButtonInput.acquire(input_server)
    Lights.set(lights_server, [false, true, true, false])

    brightness = Lights.get_brightness(lights_server)

    state = %State{
      input_server: input_server,
      lights_server: lights_server,
      brightness: brightness
    }

    {:ok, state}
  end

  @impl true
  def handle_info(msg, state)

  def handle_info({:input, pin}, state) do
    choice = ButtonInput.get_choice(pin, Application.get_env(:ex_speed_game, :button_pins))

    case choice do
      2 ->
        change_brightness(state, -@brightness_step)

      3 ->
        change_brightness(state, @brightness_step)

      _ ->
        {:stop, :normal, state}
    end
  end

  @impl true
  def terminate(_reason, state) do
    ButtonInput.release(state.input_server)
  end

  @spec change_brightness(State.t(), integer()) :: {:noreply, State.t()}
  defp change_brightness(state, step) do
    brightness = (state.brightness + step) |> max(0) |> min(255)
    Lights.set_brightness(state.lights_server, brightness)
    Logger.debug("Set light brightness to #{inspect(brightness)}")
    {:noreply, %State{state | brightness: brightness}}
  end
end
