defmodule ExSpeedGame.Game.Modes.Speed do
  use GenServer, restart: :temporary
  require Logger
  import ExSpeedGame.Utils.TypedStruct

  alias ExSpeedGame.Game.{Types, Lights, ButtonInput, Randomiser}
  alias ExSpeedGame.PubSub

  @max_queue Application.get_env(:ex_speed_game, :max_queue)

  defmodule Options do
    deftypedstruct(%{
      initial_score: integer(),
      initial_delay: integer()
    })
  end

  defmodule State do
    deftypedstruct(%{
      score: integer(),
      delay: float(),
      queue: {:queue.queue(Types.pin()), :queue.new()},
      queue_len: {integer(), 0},
      previous: {Types.choice() | nil, nil},
      active: {boolean(), true}
    })
  end

  @spec start_link(Options.t()) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(%Options{} = opts) do
    GenServer.start_link(__MODULE__, %{
      initial_score: opts.initial_score,
      initial_delay: opts.initial_delay
    })
  end

  @impl true
  @spec init(%{initial_score: integer(), initial_delay: float()}) :: {:ok, State.t()}
  def init(%{initial_score: score, initial_delay: delay}) do
    ButtonInput.acquire(ButtonInput)
    Lights.clear(Lights)
    Logger.debug("Init game with score #{score} and delay #{delay}.")

    next_delay = schedule_tick(delay)

    {:ok, %State{score: score, delay: next_delay}}
  end

  # Tick handling

  @impl true
  def handle_info(msg, state)

  def handle_info(:tick, %State{queue_len: queue_len, active: true} = state)
      when queue_len >= @max_queue do
    end_game(state)
  end

  @impl true
  def handle_info(:tick, %State{active: true} = state) do
    choice = Randomiser.get(state.previous)
    Lights.set_index(Lights, choice)
    new_queue = :queue.in(choice, state.queue)
    Logger.debug("Added new choice #{inspect(choice)} to end of queue")

    next_delay = schedule_tick(state.delay)

    new_state = %State{
      state
      | delay: next_delay,
        previous: choice,
        queue: new_queue,
        queue_len: state.queue_len + 1
    }

    PubSub.publish(new_state)
    {:noreply, new_state}
  end

  @impl true
  def handle_info(:tick, %State{active: false} = state), do: {:noreply, state}

  # Input handling

  @impl true
  def handle_info({:input, _}, %State{active: false} = state) do
    {:stop, :normal, state}
  end

  @impl true
  def handle_info({:input, pin}, %State{active: true} = state) do
    choice = ButtonInput.get_choice(pin, Application.get_env(:ex_speed_game, :button_pins))

    case :queue.peek(state.queue) do
      {:value, ^choice} ->
        score = state.score + 1
        Logger.debug("Score: #{score}")

        new_state = %State{
          state
          | queue: :queue.drop(state.queue),
            queue_len: state.queue_len - 1,
            score: score
        }

        PubSub.publish(new_state)
        {:noreply, new_state}

      val ->
        Logger.debug(
          "Game ended, got input #{inspect(choice)} but expected #{inspect(val)}. Queue: #{inspect(:queue.to_list(state.queue))}"
        )

        end_game(state)
    end
  end

  @impl true
  @spec handle_call({:esg_pubsub, :get_state}, GenServer.from(), State.t()) ::
          {:reply, State.t(), State.t()}
  def handle_call({:esg_pubsub, :get_state}, _from, %State{} = state) do
    {:reply, state, state}
  end

  @impl true
  @spec terminate(any(), any()) :: term()
  def terminate(_reason, _state) do
    ButtonInput.release(ButtonInput)
  end

  @spec get_delay_at(integer(), float()) :: float()
  def get_delay_at(score, initial_delay) do
    for _i <- 1..score, reduce: initial_delay do
      acc -> get_next_delay(acc)
    end
  end

  @spec schedule_tick(float()) :: float()
  defp schedule_tick(delay) do
    Process.send_after(self(), :tick, trunc(delay))
    get_next_delay(delay)
  end

  @spec get_next_delay(float()) :: float()
  defp get_next_delay(delay)
  defp get_next_delay(delay) when delay > 399, do: delay * 0.993
  defp get_next_delay(delay) when delay > 326, do: delay * 0.996
  defp get_next_delay(delay) when delay > 192, do: delay * 0.9985
  defp get_next_delay(delay) when delay > 1, do: delay - 1
  defp get_next_delay(delay), do: delay

  @spec end_game(State.t()) :: {:noreply, State.t()}
  defp end_game(state) do
    new_state = %State{state | active: false}

    # Don't listen to button inputs while showing failure lights
    Logger.debug("Game ended with score #{state.score}.")
    PubSub.publish(new_state)

    ButtonInput.release(ButtonInput)
    Lights.set(Lights, Application.get_env(:ex_speed_game, :fail_pattern))
    Process.sleep(Application.get_env(:ex_speed_game, :win_delay))
    ButtonInput.acquire(ButtonInput)

    {:noreply, new_state}
  end
end
