defmodule ExSpeedGame.Game.Randomiser do
  alias ExSpeedGame.Game.Types

  @choices 1..4
  @choices_size Enum.count(@choices)
  @choices_first Enum.at(@choices, 0)

  @doc """
  Get a random button choice that is not the same as the given previous choice.

  If previous given is nil, any choice can be returned.
  """
  @spec get(Types.choice() | nil) :: Types.choice()
  def get(previous)

  def get(nil), do: Enum.random(@choices)

  def get(previous) do
    pick = previous - @choices_first + Enum.random(@choices_first..(@choices_size - 1))
    rem(pick, @choices_size) + @choices_first
  end
end
