defmodule ExSpeedGame.Game.Menu do
  use GenServer
  require Logger
  import ExSpeedGame.Utils.TypedStruct

  alias ExSpeedGame.Game.{Types, ButtonInput, ShowIP, Lights, ChangeBrightness}

  alias ExSpeedGame.Game.Modes.{
    Speed
  }

  alias ExSpeedGame.PubSub

  @initial_delay Application.compile_env!(:ex_speed_game, :delay_start)
  @pro_score Application.compile_env!(:ex_speed_game, :score_pro)

  @menu {
    {"SpeedGame", Speed,
     %Speed.Options{
       initial_score: 0,
       initial_delay: @initial_delay
     }},
    {"SpeedGame Pro", Speed,
     %Speed.Options{
       initial_score: @pro_score,
       initial_delay: Speed.get_delay_at(@pro_score, @initial_delay)
     }},
    {"MemoryGame", Speed, %{}},
    {"Show IP", ShowIP, %{}},
    {"Change brightness", ChangeBrightness,
     %ChangeBrightness.Options{input_server: ButtonInput, lights_server: Lights}}
  }
  @menu_size :erlang.tuple_size(@menu)

  defmodule Options do
    deftypedstruct(%{
      name: GenServer.name()
    })
  end

  defmodule State do
    @type mode :: :menu | :ingame
    deftypedstruct(%{
      index: {integer(), 0},
      button_pins: Types.pins(),
      mode: {mode(), :menu},
      game: {pid() | nil, nil},
      game_ref: {reference() | nil, nil}
    })
  end

  @spec start_link(Options.t()) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(%Options{} = opts) do
    GenServer.start_link(
      __MODULE__,
      %{},
      name: opts.name
    )
  end

  @impl true
  @spec init(any) :: {:ok, State.t()}
  def init(_) do
    state = %State{button_pins: Application.get_env(:ex_speed_game, :button_pins)}
    ButtonInput.acquire(ButtonInput)
    display_index(state.index)
    {:ok, state}
  end

  @impl true
  def handle_info(msg, state)

  def handle_info(
        {:input, btn_pin},
        %State{index: index, button_pins: {pin1, _, _, pin4}, mode: :menu} = state
      )
      when btn_pin in [pin1, pin4] do
    is_first_button = btn_pin == pin1
    is_last_button = btn_pin == pin4

    new_index =
      cond do
        is_first_button and index > 0 -> index - 1
        is_last_button and index < @menu_size - 1 -> index + 1
        true -> index
      end

    display_index(new_index)
    new_state = %State{state | index: new_index}
    PubSub.publish(new_state)
    {:noreply, new_state}
  end

  def handle_info({:input, _pin}, %State{index: index, mode: :menu} = state) do
    ButtonInput.release(ButtonInput)

    {_, module, init_arg} = elem(@menu, index)

    {:ok, pid} =
      DynamicSupervisor.start_child(ExSpeedGame.Game.Supervisor, module.child_spec(init_arg))

    ref = Process.monitor(pid)

    new_state = %State{state | mode: :ingame, game: pid, game_ref: ref}
    PubSub.publish(new_state)
    {:noreply, new_state}
  end

  def handle_info(
        {:DOWN, ref, :process, _, reason},
        %State{
          index: index,
          mode: :ingame,
          game_ref: game_ref
        } = state
      )
      when ref == game_ref do
    PubSub.clear(state.game)

    # Ensure input is released in case process crashed violently
    ButtonInput.release(ButtonInput)

    # If crashed, show crash pattern for a moment
    if reason != :normal do
      Lights.set(Lights, Application.get_env(:ex_speed_game, :crash_pattern))
      Process.sleep(Application.get_env(:ex_speed_game, :crash_pattern_delay))
    end

    ButtonInput.acquire(ButtonInput)
    display_index(index)

    new_state = %State{state | mode: :menu, game: nil, game_ref: nil}
    PubSub.publish(new_state)
    {:noreply, new_state}
  end

  def handle_info(_, state), do: {:noreply, state}

  @impl true
  @spec handle_call({:esg_pubsub, :get_state}, GenServer.from(), State.t()) ::
          {:reply, State.t(), State.t()}
  def handle_call({:esg_pubsub, :get_state}, _from, %State{} = state) do
    {:reply, state, state}
  end

  @spec index_name(integer()) :: String.t()
  def index_name(index) do
    @menu |> elem(index) |> elem(0)
  end

  defp display_index(index) do
    Logger.debug("Menu viewing: #{index_name(index)}")
    Lights.show_binary(Lights, index + 1)
  end
end
