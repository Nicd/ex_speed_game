defmodule ExSpeedGame.Game.ShowIP do
  use GenServer, restart: :temporary
  import ExSpeedGame.Utils.TypedStruct

  require Logger

  defmodule State do
    deftypedstruct(%{
      ip: String.t(),
      index: {integer(), 0}
    })
  end

  @spec start_link(any) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(_) do
    GenServer.start_link(__MODULE__, nil)
  end

  @impl true
  @spec init(any) :: {:ok, State.t()}
  def init(_) do
    ExSpeedGame.Game.ButtonInput.acquire(ExSpeedGame.Game.ButtonInput)

    {:ok, ifaddrs} = :inet.getifaddrs()

    {_, ifaddr} =
      List.keyfind(
        ifaddrs,
        String.to_charlist(Application.get_env(:ex_speed_game, :iface)),
        0,
        []
      )

    ip = Keyword.get(ifaddr, :addr, {0, 0, 0, 0}) |> :inet.ntoa() |> to_string() |> Kernel.<>(".")
    Logger.debug("Showing IP: #{inspect(ip)}")

    state = %State{ip: ip}
    display_number(state.ip, state.index)
    {:ok, state}
  end

  @impl true
  def handle_info({:input, _pin}, %State{ip: ip, index: index} = state) do
    if index == String.length(ip) - 1 do
      {:stop, :normal, state}
    else
      index = index + 1
      display_number(ip, index)
      {:noreply, %State{state | index: index}}
    end
  end

  @impl true
  def terminate(_reason, _state) do
    ExSpeedGame.Game.ButtonInput.release(ExSpeedGame.Game.ButtonInput)
  end

  defp display_number(ip, index) do
    server = ExSpeedGame.Game.Lights

    case String.at(ip, index) do
      "." ->
        ExSpeedGame.Game.Lights.clear(server)

      int ->
        {num, ""} = Integer.parse(int)
        ExSpeedGame.Game.Lights.show_binary(server, num)
    end
  end
end
