defmodule ExSpeedGame.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  alias ExSpeedGame.Game.{
    ButtonInput,
    Lights,
    Menu
  }

  alias ExSpeedGame.Game.Supervisor, as: GameSupervisor

  def start(_type, _args) do
    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_all, max_restarts: 1_000_000, name: ExSpeedGame.Supervisor]

    children =
      [
        # Children for all targets
        # Starts a worker by calling: ExSpeedGame.Worker.start_link(arg)
        # {ExSpeedGame.Worker, arg},
      ] ++ children(target())

    Supervisor.start_link(children, opts)
  end

  # List all child processes to be supervised
  def children(_target) do
    pins = Application.get_env(:ex_speed_game, :button_pins)
    debounce_delay = Application.get_env(:ex_speed_game, :debounce_delay)
    repeat_delay = Application.get_env(:ex_speed_game, :repeat_delay)
    led_channel = Application.get_env(:ex_speed_game, :led_channel)
    led_brightness = Application.get_env(:ex_speed_game, :led_brightness)

    [
      # Starts a worker by calling: ExSpeedGame.Worker.start_link(arg)
      {Registry,
       [name: ExSpeedGame.PubSub, keys: :duplicate, partitions: System.schedulers_online()]},
      {ButtonInput,
       %ButtonInput.Options{
         pins: pins,
         debounce_delay: debounce_delay,
         repeat_delay: repeat_delay,
         name: ButtonInput
       }},
      {Lights, %Lights.Options{channel: led_channel, brightness: led_brightness, name: Lights}},
      {Menu, %Menu.Options{name: Menu}},
      {DynamicSupervisor, strategy: :one_for_one, name: GameSupervisor},

      # Web UI
      ExSpeedGame.Web.Telemetry,
      {Phoenix.PubSub, name: ExSpeedGame.Web.PubSub},
      ExSpeedGame.Web.Endpoint
    ]
  end

  def target() do
    Application.get_env(:ex_speed_game, :target)
  end
end
